#!/bin/sh

VERSION="4.1"
OUT="$1"

v="$VERSION"

if test -z "$(git rev-parse --show-cdup 2>/dev/null)" &&
   head=`git rev-parse --verify --short HEAD 2>/dev/null`; then
	v="$v"-g$head
fi

echo '#include "iw.h"' > "$OUT"
echo "const char iw_version[] = \"$v\";" >> "$OUT"
